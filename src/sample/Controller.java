package sample;


import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Ellipse;

import java.util.ArrayList;
import java.util.List;


public class Controller {


    //DO NOT TOUCH!!!
    Equipment Equip = new Equipment();
    float StartX=180;
    float StartY=140;
    Ellipse body = null;
    List<Circle> point_list = new ArrayList<>();
    /////////////////


    //Ссылки на объекты
    @FXML
    private AnchorPane centerPanel;
    @FXML
    private TextField X_coord;
    @FXML
    private TextField Y_coord;
    @FXML
    private Label voltPanel;
    @FXML
    private Button Placeonbody_id;
    @FXML
    private Button EnableVolt;
    @FXML
    private Button EnableGen_2;

    @FXML
    void initialize() {

        centerPanel.addEventHandler(MouseEvent.MOUSE_MOVED, MoveZond());
        centerPanel.addEventHandler(MouseEvent.MOUSE_CLICKED, PaintPoint());
        MakeGrid grid = new MakeGrid();
        centerPanel.getChildren().add(grid.getGrid(StartX, StartY));
    }





    @FXML
    private void ClearGrid() {
        centerPanel.getChildren().removeAll(point_list);
    }

    //Сброс всех настроек
    @FXML
    private void ResetParam() {

        Equip.VM = false;

        Equip.AC = false;
        Equip.BP = false;

        EnableVolt.setText("Подключить вольтметр");
        EnableGen_2.setText("Подключить ГН");
        centerPanel.getChildren().remove(this.body);
        this.body=null;
        Placeonbody_id.setText("Поместить проводящее тело по координатам");
        Equip.V = 14;
        X_coord.setText("");
        Y_coord.setText("");
        ClearGrid();
        voltPanel.setText("88.88");
    }


    //Подключить вольтметр
    @FXML
    private void EnableVolt(){
        if(EnableVolt.getText().equals("Подключить вольтметр"))
        {
            EnableVolt.setText("Отключить вольтметр");
            Equip.VM = true;
        }
        else
        {
            EnableVolt.setText("Подключить вольтметр");
            Equip.VM = false;
            voltPanel.setText("88.88");
        }
    }


    //Включить генератор переменного напряжения
    @FXML
    private void EnableGen_2(){
        if(EnableGen_2.getText().equals("Подключить ГН"))
        {
            EnableGen_2.setText("Отключить ГН");
            Equip.AC = true;


        }
        else
        {
            EnableGen_2.setText("Подключить ГН");
            Equip.AC = false;
        }

    }


    //Увеличить напряжение
    @FXML
    private void Up_Voltage() {
        Equip.charge.forEach(x->x.Q+=1*Math.pow(10,-14));
        Equip.charge_body.forEach(x->x.Q+=1*Math.pow(10,-14));
        Equip.Q+=1*Math.pow(10,-14);
    }

    //Уменьшить напряжение
    @FXML
    private void Down_Voltage() {
        Equip.charge.forEach(x->x.Q-=1*Math.pow(10,-14));
        Equip.charge_body.forEach(x->x.Q-=1*Math.pow(10,-14));
        Equip.Q-=1*Math.pow(10,-14);
    }



    @FXML
    private void Placeonbody() {
        if(
            !(X_coord.getText().equals(""))
            && !(Y_coord.getText().equals(""))
            && Double.parseDouble(X_coord.getText())<=20
            && Double.parseDouble(Y_coord.getText())<=15
            && Double.parseDouble(Y_coord.getText())>=0
            && Double.parseDouble(X_coord.getText())>=0
        )
        {
            if (body==null) {
                Ellipse body = new Ellipse();

                body.setStyle("-fx-background-color: rgba(255,0,0,0.3);");
                body.setOpacity(0.5);
                body.setCenterX(Double.parseDouble(X_coord.getText()) * 18 + StartX);
                body.setCenterY(StartY + 535 - Double.parseDouble(Y_coord.getText()) * 18);
                body.setRadiusX(109);
                body.setRadiusY(109);
                this.body=body;
                Placeonbody_id.setText("Убрать проводящее тело");
                centerPanel.getChildren().add(body);
                Equip.BP = true;
                Equip.xb = Double.parseDouble(X_coord.getText());
                Equip.yb = Double.parseDouble(Y_coord.getText());

            } else {
                Equip.BP = false;
                centerPanel.getChildren().remove(this.body);
                this.body=null;

                Placeonbody_id.setText("Поместить проводящее тело по координатам");
            }
        }
    }

    EventHandler<MouseEvent> MoveZond() {

        return new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                if ((e.getX() - StartX) / 18 >= 0 && (e.getX() - StartX) / 18 <= 29 && Math.abs(e.getY() - 675) / 18<=19 && Math.abs(e.getY() - 675) / 18>=0) {



                double Xzond, Yzond;
                Xzond = (e.getX() - StartX) / 18;
                Yzond=Math.abs(e.getY() - 675) / 18;


                if(Equip.AC) {

                    if(Equip.VM )
                    voltPanel.setText(String.format("%.2f",Equip.GetVolt200211(Xzond, Yzond)).replace(",", "."));
                }
                }
            }

        };
    }

    EventHandler<MouseEvent> PaintPoint() {
        return e -> {
            if(e.getButton() == MouseButton.PRIMARY && e.getX()<673 && e.getY()>330) {
                Circle point = new Circle();
                point.setLayoutX(e.getX());
                point.setLayoutY(e.getY());
                point.setRadius(2);

                centerPanel.getChildren().add(point);
                point_list.add(point);
            }
            else if(e.getButton() == MouseButton.SECONDARY) {
                try {
                    centerPanel.getChildren().forEach(x -> {
                        if (x.getClass() == Circle.class && x.getLayoutX() < e.getX() + 5 && x.getLayoutX() > e.getX() - 5 && x.getLayoutY() < e.getY() + 5 && x.getLayoutY() > e.getY() - 5) {
                            centerPanel.getChildren().remove(x);
                        }
                    });
                } catch(Exception ignored){}
            }
        };
    }
}
