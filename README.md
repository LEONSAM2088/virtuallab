# VirtualLab

The virtual laboratory for familiarizing yourself with measurement equipment

## Features
- Place or remove the conductive body in an electric bath
- Increase or decrease the output voltage of the generator
- Move the probe vertically on the scale grid to measure the potential



## Requirements
- javafx-sdk
