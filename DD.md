# Design Document

## Функциональные детали
- Присутсвует только одна группа пользователей
- Пользователям доступны все функции приложения
- [Бизнес процессы](https://miro.com/app/board/o9J_lAiRdIs=/?moveToWidget=3074457359684373358&cot=14)

## Структура проекта
- Структура проекта представлена на [доске miro](https://miro.com/app/board/o9J_lAiRdIs=/?moveToWidget=3074457359686352878&cot=14)
- [Дизайн интерфейса взаимодействия](https://miro.com/app/board/o9J_lAiRdIs=/?moveToWidget=3074457359688032799&cot=14)

## План выполнения

- Front-end разработка(2 дня) => design
<pre>Добавить окно, кнопки, методы вызывемые нажатием кнопок, панели вывода, сетку, возможность ставить и убирать точки</pre>
- Написание Back-end(10 часов) => physicist
<pre>Настроить показания вольтметра и амперметра в зависимости от положения зонда на сетке и при наличии проводящего тела</pre>
- Взаимоинтеграция Front-end и Back-end(1 час) => optimizer
<pre>Связать классы</pre>
- Рефакторинг кода(1 час)
<pre>Оптимизировать код, избавиться от предупреждений</pre>
