package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.shape.Line;
import javafx.stage.Stage;

import java.io.InputStream;
import java.net.URL;
import java.util.Objects;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{


        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("sample.fxml")));
        primaryStage.setTitle("VirtualLab");
        primaryStage.setScene(new Scene(root, 1100, 700));

        primaryStage.setResizable(false);



        //Иконка
        InputStream iconStream = getClass().getResourceAsStream("../materials/icon.png");
        assert iconStream != null;
        Image image = new Image(iconStream);
        primaryStage.getIcons().add(image);

        //css
        String stylesheet = Objects.requireNonNull(getClass().getResource("../css/style.css")).toExternalForm();
        root.getStylesheets().add(stylesheet);




        primaryStage.show();


    }


    public static void main(String[] args) {
        launch(args);
    }
}
