package sample;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.sqrt;

public class Equipment {


    public int L = 2;

    //Электропроводность раствора
    public double V=14; //выходное напряжение генератора
    public double K=  1/(4*Math.PI*8.854*Math.pow(10,-12));
    public double Q=5*Math.pow(10,-14);
    List<Charge> charge = new ArrayList<>();
    List<Charge> charge_body = new ArrayList<>();

    public boolean AC=false;


    public boolean VM=false;
    public boolean BP=false;
    public double xb,yb; //координаты кольца



    Equipment() {
        int j=4;
        for(int i=-40*j; i<40*j; i++)
            charge.add(new Charge(-7,i/j, -Q));
        for(int i=-40*j; i<40*j; i++)
            charge.add(new Charge(31,i/j, Q));


    }


    boolean addedchargesbody=false;
    public Double GetVolt200211(double xz , double yz){

        if(!BP)
        {


            return Math.abs(K * charge.stream().mapToDouble(z -> (z.Q / getDistance(z.X, z.Y, 0.2, 10.0))).sum()- K * charge.stream().mapToDouble(     z -> z.Q / (getDistance(z.X, z.Y, xz, yz))      ).sum());
        }
        else {

           if(!addedchargesbody) {
                for (int i = 90; i < 270; i=i+30) {
                    charge_body.add(new Charge(xb + 6 * Math.cos((Math.PI / 180) * i), yb + 6 * Math.sin((Math.PI / 180) * i), Q));

                }
                for (int i = 0; i < 90; i=i+30) {
                    charge_body.add(new Charge(xb + 6 * Math.cos((Math.PI / 180) * i), yb + 6 * Math.sin((Math.PI / 180) * i), -Q));

                }
                for (int i = 270; i < 360; i=i+30) {
                    charge_body.add(new Charge(xb + 6 * Math.cos((Math.PI / 180) * i), yb + 6 * Math.sin((Math.PI / 180) * i), -Q));

                }
                addedchargesbody=true;
            }

        return Math.abs(K * (charge.stream().mapToDouble(z -> (z.Q / getDistance(z.X, z.Y, 0.2, 10.0))).sum()+charge_body.stream().mapToDouble(z -> (z.Q / getDistance(z.X, z.Y, 0.2, 10.0))).sum())- K * (charge.stream().mapToDouble(     z -> z.Q / (getDistance(z.X, z.Y, xz, yz))      ).sum()+charge_body.stream().mapToDouble(     z -> z.Q / (getDistance(z.X, z.Y, xz, yz))      ).sum()));


        }
    }


    double getDistance(double x1, double y1, double x2, double y2){
        return sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2))/1000;
    }
}
