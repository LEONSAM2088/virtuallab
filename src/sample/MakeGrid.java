package sample;

import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.shape.Line;

public class MakeGrid {
   public Group getGrid(float StartX, float StartY) {
        Group group = new Group();
        float y=StartY + 193;
        for (int i=20; i>0; i--){
            Line line = new Line(StartX, y, 523+StartX, y);
            Label lbl = new Label();
            lbl.setText(String.valueOf(i-2));
            lbl.setLayoutX(StartX-20);
            lbl.setLayoutY(y+10);
            if(i%2==0)   group.getChildren().add(lbl);
            group.getChildren().add(line);
            y+=18;
        }

        float x=StartX;
        for (int i=1; i<=30; i++){
            Line line = new Line(x, StartY + 193, x,StartY+535);
            group.getChildren().add(line);

            Label lblH = new Label(String.valueOf(i-2));
            lblH.setLayoutX(x-21);
            lblH.setLayoutY(StartY+535+5);
            if(i%2==0)   group.getChildren().add(lblH);

            x+=18;
        }
        Line leftEl = new Line(StartX, StartY + 216, StartX,StartY+512);
        leftEl.setScaleX(10);
        leftEl.setOpacity(0.5);
        group.getChildren().add(leftEl);
        Line rightEl = new Line(StartX+522, StartY + 216, StartX+522,StartY+512);
        rightEl.setScaleX(10);
        rightEl.setOpacity(0.5);
        group.getChildren().add(rightEl);
        return group;
    }
}
